    public static int facing = 1;
    
    public static final int up = 0;
    public static final int right = 1;
    public static final int down = 2;
    public static final int left = 3;
    
    public static final int move = 1;
    public static final int moveSafe = 2;
    public static final int isMovePossible = 3;
    public static final int turnLeft = 4;
    public static final int turnRight = 5;
    public static final int pickUp = 6;
    public static final int moveUp = 10;
    public static final int moveRight = 11;
    public static final int moveDown = 12;
    public static final int moveLeft = 13;
    public static final int next = 20;
    public static final int nextUp = 20;
    public static final int nextRight = 21;
    public static final int nextDown = 22;
    public static final int nextLeft = 23;
    public static final int isSafeUp = 24;
    public static final int isSafeRight = 25;
    public static final int isSafeDown = 26;
    public static final int isSafeLeft = 27;
    public static final int face = 30;
    public static final int faceUp = 30;
    public static final int faceRight = 31;
    public static final int faceDown = 32;
    public static final int faceLeft = 33;
    public static final int faceOpposite = 34;
    
    public java.util.function.Function<Integer, Object> j;
    
    public void run() {
        j = action -> {
            int oldF = facing;
            switch (action) {
                case move: move(); return null;
                case moveUp:
                case moveRight:
                case moveDown:
                case moveLeft:
                    j.apply(face + action - moveUp);
                    return j.apply(moveSafe);
                case moveSafe:
                    boolean isSafe1 = (boolean) j.apply(isMovePossible);
                    if (isSafe1) j.apply(move);
                    return isSafe1;
                case isSafeUp:
                case isSafeRight:
                case isSafeDown:
                case isSafeLeft:
                    j.apply(face + action - isSafeUp);
                    Object isSafe2 = j.apply(isMovePossible);
                    j.apply(face + oldF);
                    return isSafe2;
                case turnLeft: turnLeft(); facing = (facing + 3) % 4; return facing;
                case turnRight: turnRight(); facing = (facing + 1) % 4; return facing;
                case isMovePossible: return isMovePossible();
                case pickUp: pickUp(); return null;
                case faceUp:
                case faceRight:
                case faceDown:
                case faceLeft:
                    int targetFace = action - face;
                    if (targetFace == (facing + 3) % 4) j.apply(turnLeft);
                    else while (facing != targetFace) j.apply(turnRight);
                    return null;
                case faceOpposite: j.apply(turnRight); j.apply(turnRight); return facing;
                case nextUp:
                case nextRight:
                case nextDown:
                case nextLeft:
                    boolean hasMovedInDirection = (boolean) j.apply(moveUp + action - nextUp);
                    j.apply(face + oldF);
                    if (!hasMovedInDirection) j.apply(moveSafe);
                    return hasMovedInDirection;
            }
            return null;
        };
        
        while (!(boolean) j.apply(isSafeDown)) { j.apply(moveRight); }
        
    }