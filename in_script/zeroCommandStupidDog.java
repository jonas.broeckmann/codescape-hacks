    public void run() {
        stupidDog = this;
        stupidDog.execute("Your commands here");
    }
    
    MyDogbot stupidDog;
    
    /**
     * Executes a series of commands without effecting the "Commands-Counter".
     * Returns a map of all results as strings (if the key is not specified, a default index will be used).
     * Commands (case insensitive):
     * 
     * m        -> move
     * e        -> move as far as possible
     * s        -> move if possible
     * i        -> is move possible (adds "1" or "0" to the result map)
     * i{key}   -> is move possible (adds "1" or "0" to the result map with key "key")
     * l        -> turn left
     * r        -> turn right
     * p        -> pick up stuff
     * t        -> rest
     * w(stuff) -> write "stuff" (example: "w4(dog)" = writes "dog" 4 times)
     * w{key}   -> write contents of result at key "key" (example: "d{pass}l2w{pass}" = read pass from terminal and write the value of pass)
     * d        -> read and add the value to the result map
     * d{key}   -> read and add the value to the result map with key "key"
     * 
     * An optional number after a command indicates how often it should get executed ("0","1","2",...,"9").
     * 
     * Example for level 1: stupidDog.execute("epl2m4re");
     * Example for write: stupidDog.execute("mlw(activate)re");
     * Example for "50/50-Chance": stupidDog.execute(stupidDog.execute("ldrm").get("0") + "m");
     */
    public java.util.Map<String, String> execute(String command) {
        java.util.HashMap<String, String> results = new java.util.HashMap<>();
        int rIndex = 0;
        int l = command.length();
        for (int i = 0; i < l; i++) {
            char c = command.charAt(i);
            int n = (i + 1 != l && Character.isDigit(command.charAt(i + 1))) ? Character.getNumericValue(command.charAt(i + 1)) : -4;
            if (n != -4) i++;
            if (n < 0) n = 1;
            System.out.println(c + "," + n);
            for(int j = n; j > 0; j--) switch (Character.toLowerCase(c)) {
                case 'm': stupidDog.move(); break;
                case 'e': while(stupidDog.isMovePossible()) stupidDog.move(); break;
                case 's': if (stupidDog.isMovePossible()) stupidDog.move(); break;
                case 'i':
                    String iKey = "" + rIndex++;
                    java.util.regex.Matcher iFlag = java.util.regex.Pattern.compile("^.?\\{([^\\}]*)\\}", java.util.regex.Pattern.MULTILINE).matcher(command.substring(i));
                    if (iFlag.find()) {
                        i += 2 + iFlag.group(1).length();
                        iKey = iFlag.group(1);
                    }
                    System.out.println("i to key \""+iKey+"\"");
                    results.put(iKey, stupidDog.isMovePossible() ? "1" : "0"); break;
                case 'l': stupidDog.turnLeft(); break;
                case 'r': stupidDog.turnRight(); break;
                case 'p': stupidDog.pickUp(); break;
                case 't': stupidDog.rest(); break;
                case 'w':
                    String wSub = command.substring(i);
                    java.util.regex.Matcher wParam1 = java.util.regex.Pattern.compile("\\(([^)]*)\\)", java.util.regex.Pattern.MULTILINE).matcher(wSub);
                    if (wParam1.find()) {
                        i += 2 + wParam1.group(1).length();
                        stupidDog.write(wParam1.group(1));
                        break;
                    }
                    java.util.regex.Matcher wParam2 = java.util.regex.Pattern.compile("^.?\\{([^\\}]*)\\}", java.util.regex.Pattern.MULTILINE).matcher(wSub);
                    if (wParam2.find()) {
                        i += 2 + wParam2.group(1).length();
                        if (!results.containsKey(wParam2.group(1))) throw new NullPointerException("Undefined key: " + wParam2.group(1));
                        stupidDog.write(results.get(wParam2.group(1)));
                    }
                    break;
                case 'd':
                    String readKey = "" + rIndex++;
                    java.util.regex.Matcher dFlag = java.util.regex.Pattern.compile("^.?\\{([^\\}]*)\\}", java.util.regex.Pattern.MULTILINE).matcher(command.substring(i));
                    if (dFlag.find()) {
                        i += 2 + dFlag.group(1).length();
                        readKey = dFlag.group(1);
                    }
                    System.out.println("read to key \""+readKey+"\"");
                    results.put(readKey, stupidDog.read());
                    break;
            }
        }
        return results;
    }