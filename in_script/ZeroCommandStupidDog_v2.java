    // NOT COMPLETE
    
    public void run() {
        StupidDogSolver yeet = new StupidDogSolver(this, "right");
        yeet.execute("Your commands here");
    }
    
    /**
     * Executes a series of commands without effecting the "Commands-Counter".
     * Returns a map of all results as strings (if the key is not specified, a default index will be used).
     * Commands (case insensitive):
     * 
     * m        -> move
     * e        -> move as far as possible
     * s        -> move if possible
     * i        -> is move possible (adds "1" or "0" to the result map)
     * i[key]   -> is move possible (adds "1" or "0" to the result map with key "key")
     * l        -> turn left
     * r        -> turn right
     * p        -> pick up stuff
     * t        -> rest
     * w(stuff) -> write "stuff" (example: "w4(dog)" = writes "dog" 4 times)
     * w[key]   -> write contents of result at key "key" (example: "d[pass]l2w[pass]" = read pass from terminal and write the value of pass)
     * d        -> read and add the value to the result map
     * d[key]   -> read and add the value to the result map with key "key"
     * 
     * An optional number after a command indicates how often it should get executed ("0","1","2",...,"9").
     * 
     * Example for level 1: stupidDog.execute("epl2m4re");
     * Example for write: stupidDog.execute("mlw(activate)re");
     * Example for "50/50-Chance": stupidDog.execute(stupidDog.execute("ldrm").get("0") + "m");
     */
    public class StupidDogSolver {
        private StupidDogSolver self;
        private Dogbot stupidDog;
        private int orientation = 0;
        private java.util.Map<String, String> data;
        
        public StupidDogSolver(Dogbot stupidDog, String startOrientation) {
            self = this;
            this.stupidDog = stupidDog;
            orientation = self.orientationFromString(startOrientation, 0);
            data = new java.util.HashMap<>();
        }
        
        public java.util.Map<String, String> execute(String command) {
            java.util.HashMap<String, String> tmpData = new java.util.HashMap<>();
            int keyIndex = 0;
            String regex = "(?:([mlrpdwei])(?:\\(([^)]*)\\)|\\[([^\\]]*)\\]){0,2}([0-9]+)?)";
            java.util.regex.Matcher matcher = java.util.regex.Pattern.compile(regex, java.util.regex.Pattern.MULTILINE | java.util.regex.Pattern.CASE_INSENSITIVE).matcher(command);
            while (matcher.find()) {
                String c = matcher.group(1);
                String param = matcher.group(2);
                String ref = matcher.group(3);
                int count = matcher.group(4) != null ? Integer.parseInt(matcher.group(4)) : 1;
                System.out.println(matcher.group(0) + ";");
                for (int i = count; i > 0; i--) switch (c) {
                    case "m": self.move(self.orientationFromString(param, orientation), false); break;
                    case "f": self.face(self.orientationFromString(param, orientation)); break;
                    case "e": self.face(self.orientationFromString(param, orientation)); while(stupidDog.isMovePossible()) self.move(self.orientationFromString(param, orientation), false); break;
                    case "s": self.move(self.orientationFromString(param, orientation), true); break;
                    case "i": keyIndex = addTmpData(stupidDog.isMovePossible() ? "1" : "0", param != null ? param : ref, keyIndex, tmpData); break;
                    case "l": self.turnLeft(); break;
                    case "r": self.turnRight(); break;
                    case "p": self.pickUp(); break;
                    case "t": stupidDog.rest(); break;
                    case "w": stupidDog.write(param != null ? param : tmpData.get(ref != null ? ref : "" + (keyIndex - 1))); break;
                    case "d": keyIndex = addTmpData(stupidDog.read(), param != null ? param : ref, keyIndex, tmpData); break;
                }
            }
            return tmpData;
        }
        
        private int addTmpData(String value, String ref, int keyIndex, java.util.HashMap<String, String> tmpData) {
            if (ref != null) {
                tmpData.put(ref, value);
                addData(ref, value);
            } else tmpData.put("" + keyIndex++, value);
            return keyIndex;
        }
        
        public void addData(String key, String value) {
            data.put(key, value);
        }
        public String getData(String key) {
            return data.get(key);
        }
        
        private int orientationFromString(String str, int def) {
            if (str != null) switch (str) {
                case "u":
                case "up":
                    return 0;
                case "d":
                case "down":
                    return 2;
                case "l":
                case "left":
                    return 3;
                case "r":
                case "right":
                    return 1;
            }
            return def;
        }
        
        public void move() {
            self.move(-1, false);
        }
        public void move(boolean onlyIfSafe) {
            self.move(-1, onlyIfSafe);
        }
        public void move(int direction, boolean onlyIfSafe) {
            if (direction < 0) direction = orientation;
            self.face(direction);
            if (!onlyIfSafe || stupidDog.isMovePossible()) stupidDog.move();
        }
        public void face(int direction) {
            if ((orientation + 3) % 4 == direction) self.turnLeft();
            while (orientation != direction) self.turnRight();
        }
        
        public void turnLeft() {
            stupidDog.turnLeft();
            orientation = (orientation + 3) % 4;
        }
        public void turnRight() {
            stupidDog.turnRight();
            orientation = (orientation + 1) % 4;
        }
        
        public void pickUp() {
            stupidDog.pickUp();
        }
    }
