        final int move = 1;
        final int turnLeft = 2;
        final int turnRight = 3;
        final int pickUp = 4;
        java.util.function.Function<Integer, Object> j = action -> {
            switch (action) {
                case move: move(); return null;
                case turnLeft: turnLeft(); return null;
                case turnRight: turnRight(); return null;
                case pickUp: pickUp(); return null;
            }
            return null;
        };