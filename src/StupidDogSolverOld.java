import codescape.Dogbot;

/**
 * Executes a series of commands without effecting the "Commands-Counter".
 * Returns a map of all results as strings (if the key is not specified, a default index will be used).
 * Commands (case insensitive):
 * <p>
 * m        -> move
 * e        -> move as far as possible
 * s        -> move if possible
 * i        -> is move possible (adds "1" or "0" to the result map)
 * i[key]   -> is move possible (adds "1" or "0" to the result map with key "key")
 * l        -> turn left
 * r        -> turn right
 * p        -> pick up stuff
 * t        -> rest
 * w(stuff) -> write "stuff" (example: "w4(dog)" = writes "dog" 4 times)
 * w[key]   -> write contents of result at key "key" (example: "d[pass]l2w[pass]" = read pass from terminal and write the value of pass)
 * d        -> read and add the value to the result map
 * d[key]   -> read and add the value to the result map with key "key"
 * <p>
 * An optional number after a command indicates how often it should get executed ("0","1","2",...,"9").
 * <p>
 * Example for level 1: stupidDog.execute("epl2m4re");
 * Example for write: stupidDog.execute("mlw(activate)re");
 * Example for "50/50-Chance": stupidDog.execute(stupidDog.execute("ldrm").get("0") + "m");
 */
public class StupidDogSolverOld {

    private StupidDogSolverOld self;
    private Dogbot stupidDog;
    private int orientation = 0;
    private java.util.Map<String, String> data;

    public StupidDogSolverOld(Dogbot stupidDog, String startOrientation) {
        self = this;
        this.stupidDog = stupidDog;
        orientation = self.orientationFromString(startOrientation, 0);
        data = new java.util.HashMap<>();
    }

    public java.util.Map<String, String> execute(String command) {
        java.util.HashMap<String, String> tmpData = new java.util.HashMap<>();
        int keyIndex = 0;
        java.util.regex.Matcher matcher = java.util.regex.Pattern.compile("", java.util.regex.Pattern.MULTILINE | java.util.regex.Pattern.CASE_INSENSITIVE).matcher(command);
        while (matcher.find()) {
            String c = matcher.group(1);
            String param = matcher.group(2);
            String ref = matcher.group(3);
            int count = matcher.group(4) != null ? Integer.parseInt(matcher.group(4)) : 1;
//            System.out.println(matcher.group(0) + ";");
            for (int i = count; i > 0; i--)
                switch (c) {
                case "m": self.move(self.orientationFromString(param, orientation), false); break;
                case "f": self.face(self.orientationFromString(param, orientation)); break;
                case "e": self.face(self.orientationFromString(param, orientation)); while (stupidDog.isMovePossible()) self.move(self.orientationFromString(param, orientation), false); break;
                case "s": self.move(self.orientationFromString(param, orientation), true); break;
                case "i": keyIndex = addTmpData(stupidDog.isMovePossible() ? "1" : "0", param != null ? param : ref, keyIndex, tmpData); break;
                case "l": self.turnLeft(); break;
                case "r": self.turnRight(); break;
                case "p": self.pickUp(); break;
                case "t": stupidDog.rest(); break;
                case "w": stupidDog.write(param != null ? param : tmpData.get(ref != null ? ref : "" + (keyIndex - 1))); break;
                case "d": keyIndex = addTmpData(stupidDog.read(), param != null ? param : ref, keyIndex, tmpData); break;
            }
        }
        return tmpData;
    }

    private int addTmpData(String value, String ref, int keyIndex, java.util.HashMap<String, String> tmpData) {
        if (ref != null) {
            tmpData.put(ref, value);
            addData(ref, value);
        } else tmpData.put("" + keyIndex++, value);
        return keyIndex;
    }

    public void addData(String key, String value) {
        data.put(key, value);
    }

    public String getData(String key) {
        return data.get(key);
    }

    private int orientationFromString(String str, int def) {
        if (str != null) switch (str) {
            case "u": case "up":
                return 0; case "d": case "down": return 2; case "l": case "left": return 3; case "r": case "right": return 1;
        }
        return def;
    }

    public void move() {
        self.move(-1, false);
    }
    public void move(boolean onlyIfSafe) {
        self.move(-1, onlyIfSafe);
    }
    public void move(int direction, boolean onlyIfSafe) {
        if (direction < 0) direction = orientation;
        self.face(direction);
        if (!onlyIfSafe || stupidDog.isMovePossible()) stupidDog.move();
    }

    public void face(int direction) {
        if ((orientation + 3) % 4 == direction) self.turnLeft();
        while (orientation != direction) self.turnRight();
    }

    public void turnLeft() {
        stupidDog.turnLeft();
        orientation = (orientation + 3) % 4;
    }

    public void turnRight() {
        stupidDog.turnRight();
        orientation = (orientation + 1) % 4;
    }

    public void pickUp() {
        stupidDog.pickUp();
    }
    public boolean isMovePossible() {
        return stupidDog.isMovePossible();
    }
    public String read() { return stupidDog.read(); }
    public void write(String value) { stupidDog.write(value); }
    public void rest() { stupidDog.rest(); }

    private interface SDSCommandScope {
        StupidDogSolverOld getSDS();
        void addCommands(SDSCommand... commands);
        void execute();
        java.util.Map<String, String> getAllData();
        void addData(String value);
        void addData(String value, String key);
        String getData(String key);
    }


    public static class MainCommandScope implements SDSCommandScope {
        private final StupidDogSolverOld sds;
        private final java.util.List<SDSCommand> commands = new java.util.ArrayList<>();
        private final java.util.Map<String, String> data = new java.util.HashMap<>();
        private int dataIndex = 4;

        public MainCommandScope(StupidDogSolverOld sds) {
            this.sds = sds;
        }

        @Override
        public StupidDogSolverOld getSDS() {
            return sds;
        }

        @Override
        public void addCommands(SDSCommand... commands) {
            for (SDSCommand command : commands) {
                command.setScope(this);
                this.commands.add(command);
            }
        }

        public void execute() {
            for (SDSCommand command : commands) command.executeAll();
        }

        @Override
        public java.util.Map<String, String> getAllData() {
            return data;
        }
        public void addData(String value) {
            (this).addData(value, String.valueOf(dataIndex));
        }
        public void addData(String value, String key) {
            if (key == null) {
                (this).addData(value);
                return;
            }
            data.put(key, value);
            dataIndex++;
        }
        @Override
        public String getData(String key) {
            return data.get(key);
        }
    }

    private static abstract class SDSCommand {
        private SDSCommandScope scope;
        private String reference = null;
        private String parameter = null;
        private int iterations = 1;
        private final String debugName;

        protected SDSCommand(String debugName) {
            this.debugName = debugName;
        }

        public StupidDogSolverOld getSDS() {
            return scope.getSDS();
        }

        public SDSCommandScope getScope() {
            return scope;
        }
        public void setScope(SDSCommandScope scope) {
            this.scope = scope;
        }

        public String getRef() {
            return reference;
        }
        public void setRef(String reference) {
            this.reference = reference;
        }
        public String getRefValue() {
            return (this).getScope().getData(reference);
        }
        public void setRefValue(String value) {
            (this).getScope().addData(value, reference);
        }

        public String getParam() {
            return parameter;
        }
        public void setParam(String parameter) {
            this.parameter = parameter;
        }

        public String getParamOrRefValue() {
            return (this).getParam() != null ? (this).getParam() : (this).getRefValue();
        }

        public int getIterations() {
            return iterations;
        }
        public void setIterations(int iterations) {
            this.iterations = iterations;
        }

        public void executeAll() {
            for (int i = 0; i < iterations; i++) {
                System.out.println("Executing: " + debugName);
                (this).execute();
            }
        }
        protected abstract void execute();
    }
    private static class SubCommand extends SDSCommand implements SDSCommandScope {
        private final java.util.List<SDSCommand> commands = new java.util.ArrayList<>();

        private SubCommand() {
            super("sub");
        }

        @Override
        public void addCommands(SDSCommand... commands) {
            for (SDSCommand command : commands) {
                command.setScope(this);
                this.commands.add(command);
            }
        }

        @Override
        public void execute() {
            for (SDSCommand command : commands) command.executeAll();
        }

        @Override
        public java.util.Map<String, String> getAllData() {
            return (this).getScope().getAllData();
        }
        @Override
        public void addData(String value) {
            (this).getScope().addData(value);
        }
        @Override
        public void addData(String value, String key) {
            (this).getScope().addData(value, key);
        }
        @Override
        public String getData(String key) {
            return (this).getScope().getData(key);
        }
    }


    private interface SDSCommandCreator {
        SDSCommand create();
    }

    private static class SDSCMove extends SDSCommand {
        SDSCMove() { super("move"); }
        @Override protected void execute() {
            (this).getSDS().face((this).getSDS().orientationFromString((this).getParamOrRefValue(), (this).getSDS().orientation));
            (this).getSDS().move(false);
        }
    }
    private static class SDSCMoveEnd extends SDSCommand {
        SDSCMoveEnd() { super("move_end"); }
        @Override protected void execute() {
            (this).getSDS().face((this).getSDS().orientationFromString((this).getParamOrRefValue(), (this).getSDS().orientation));
            while ((this).getSDS().isMovePossible()) (this).getSDS().move(false);
        }
    }
    private static class SDSCMoveSave extends SDSCommand {
        SDSCMoveSave() { super("move_save"); }
        @Override protected void execute() {
            (this).getSDS().face((this).getSDS().orientationFromString((this).getParamOrRefValue(), (this).getSDS().orientation));
            (this).getSDS().move(true);
        }
    }
    private static class SDSCFace extends SDSCommand {
        SDSCFace() { super("face"); }
        @Override protected void execute() {
            (this).getSDS().face((this).getSDS().orientationFromString((this).getParamOrRefValue(), (this).getSDS().orientation));
        }
    }
    private static class SDSCTurnLeft extends SDSCommand {
        SDSCTurnLeft() { super("turn_left"); }
        @Override protected void execute() {
            (this).getSDS().turnLeft();
        }
    }
    private static class SDSCTurnRight extends SDSCommand {
        SDSCTurnRight() { super("turn_right"); }
        @Override protected void execute() {
            (this).getSDS().turnRight();
        }
    }
    private static class SDSCPickUp extends SDSCommand {
        SDSCPickUp() { super("pick_up"); }
        @Override protected void execute() {
            (this).getSDS().pickUp();
        }
    }
    private static class SDSCRead extends SDSCommand {
        SDSCRead() { super("read"); }
        @Override protected void execute() {
            (this).setRefValue((this).getSDS().read());
        }
    }
    private static class SDSCWrite extends SDSCommand {
        SDSCWrite() { super("write"); }
        @Override protected void execute() {
            (this).getSDS().write((this).getParamOrRefValue());
        }
    }
    private static class SDSCCheckMove extends SDSCommand {
        SDSCCheckMove() { super("check_move"); }
        @Override protected void execute() {
            (this).setRefValue((this).getSDS().isMovePossible() ? "1" : "0");
        }
    }
    private static class SDSCRest extends SDSCommand {
        SDSCRest() { super("rest"); }
        @Override protected void execute() {
            (this).getSDS().rest();
        }
    }

    private enum SDSCommandTypes {
        MOVE("m"),
        MOVE_END("e"),
        MOVE_SAVE("s"),
        FACE("f"),
        TURN_LEFT("l"),
        TURN_RIGHT("r"),
        PICK_UP("p"),
        READ("d"),
        WRITE("w"),
        CHECK_MOVE("i"),
        REST("t");

        public final String symbol;

        SDSCommandTypes(String symbol) {
            this.symbol = symbol;
        }
        private SDSCommand createInstance() {
            switch (this) {
                case MOVE: return new SDSCMove();
                case MOVE_END: return new SDSCMoveEnd();
                case MOVE_SAVE: return new SDSCMoveSave();
                case FACE: return new SDSCFace();
                case TURN_LEFT: return new SDSCTurnLeft();
                case TURN_RIGHT: return new SDSCTurnRight();
                case PICK_UP: return new SDSCPickUp();
                case READ: return new SDSCRead();
                case WRITE: return new SDSCWrite();
                case CHECK_MOVE: return new SDSCCheckMove();
                case REST: return new SDSCRest();
                default: return null;
            }
        }
        public SDSCommand createInstance(String val, String ref, int iter) {
            SDSCommand instance = (this).createInstance();
            instance.setParam(val);
            instance.setRef(ref);
            instance.setIterations(iter);
            return instance;
        }

    }

    public static class SDSCommandParser {

        private static final String SDS_REGEX = "^(?:(?:(?:(?<command>[meslrpdwift])(?:\\((?<val>[^)]*)\\)|\\[(?<ref>[^]]*)\\]){0,2})(?<iter>[0-9]+)?)|(?<sub>\\{))";

        public static MainCommandScope parse(StupidDogSolverOld sds, String commandString) {
            MainCommandScope mainScope = new MainCommandScope(sds);
            SDSCommandParser.parseToScope(commandString, mainScope);
            return mainScope;
        }

        private static SDSCommand createCommand(String commandSymbol, String val, String ref, int iter) {
            for (SDSCommandTypes commandType : SDSCommandTypes.values())
                if (java.util.Objects.equals(commandType.symbol, commandSymbol)) return commandType.createInstance(val, ref, iter);
            throw new SDSCommandParseException("Unknown symbol '" + commandSymbol + "'");
        }

        private static SubCommand parseSubCommand(String subString, int iter) {
            SubCommand subCommand = new SubCommand();
            subCommand.setIterations(iter);
            SDSCommandParser.parseToScope(subString, subCommand);
            return subCommand;
        }

        private static void parseToScope(String str, SDSCommandScope scope) {
            java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(SDS_REGEX, java.util.regex.Pattern.MULTILINE | java.util.regex.Pattern.CASE_INSENSITIVE);
            java.util.regex.Matcher matcher = pattern.matcher(str);
            while (matcher.find()) {
                if (matcher.group("sub") != null) {
                    char[] chars = str.toCharArray();
                    int i = 0, b = 0;
                    do {
                        if (chars[i] == '{') b++;
                        else if (chars[i] == '}') b--;
                        i++;
                    } while (b != 0 && i <= chars.length);
                    int subLength = i - 2;
                    int iter = 0;
                    while (i < chars.length && Character.isDigit(chars[i])) iter = iter * 10 + Character.getNumericValue(chars[i++]);

                    //results.add(new String[] {str.substring(1, subLength + 1), String.valueOf(iter)});
                    scope.addCommands(SDSCommandParser.parseSubCommand(str.substring(1, subLength + 1), iter));

                    matcher = pattern.matcher(str = str.substring(i));
                } else {
//                    results.add(new String[] {
//                            matcher.group("command"),
//                            matcher.group("val"),
//                            matcher.group("ref"),
//                            matcher.group("iter")});
                    String command = matcher.group("command");
                    String val = matcher.group("val");
                    String ref = matcher.group("ref");
                    String iter = matcher.group("iter");
                    System.out.println("Command found: " + command + ", " + val + ", " + ref + ", " + iter);
                    scope.addCommands(createCommand(command, val, ref, iter == null ? 1 : Integer.parseInt(iter)));
                    matcher = pattern.matcher(str = str.substring(matcher.end()));
                }

            }
        }
    }

    public static class SDSCommandParseException extends RuntimeException {
        public SDSCommandParseException() { }
        public SDSCommandParseException(String message) { super(message); }
    }
}
