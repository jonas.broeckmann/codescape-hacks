package codescape;

public class Dogbot {
    private static final boolean DEBUG_COMMANDS = true;
    private static void debug(String command) {
        if (DEBUG_COMMANDS) System.out.println(command);
    }
    public void move() {
        debug("move");
    }
    public void turnLeft() {
        debug("turnLeft");
    }
    public void turnRight() {
        debug("turnRight");
    }
    public boolean isMovePossible() {
        debug("isMovePossible");
        return false;
    }
    public void rest() {
        debug("rest");
    }
    public void pickUp() {
        debug("pickUp");
    }
    public String read() {
        debug("read");
        return "help me";
    }
    public void write(String str) {
        debug("write");
        System.out.println("Writing: \"" + str + "\"");
    }
}
