import codescape.Dogbot;

/**
 * Executes a series of commands without effecting the "Commands-Counter".
 * Returns a map of all results as strings (if the key is not specified, a default index will be used).
 * Commands (case insensitive):
 * <p>
 * m        -> move
 * e        -> move as far as possible
 * s        -> move if possible
 * i        -> is move possible (adds "1" or "0" to the result map)
 * i[key]   -> is move possible (adds "1" or "0" to the result map with key "key")
 * l        -> turn left
 * r        -> turn right
 * p        -> pick up stuff
 * t        -> rest
 * w(stuff) -> write "stuff" (example: "w4(dog)" = writes "dog" 4 times)
 * w[key]   -> write contents of result at key "key" (example: "d[pass]l2w[pass]" = read pass from terminal and write the value of pass)
 * d        -> read and add the value to the result map
 * d[key]   -> read and add the value to the result map with key "key"
 * <p>
 * An optional number after a command indicates how often it should get executed ("0","1","2",...,"9").
 * <p>
 * Example for level 1: stupidDog.execute("epl2m4re");
 * Example for write: stupidDog.execute("mlw(activate)re");
 * Example for "50/50-Chance": stupidDog.execute(stupidDog.execute("ldrm").get("0") + "m");
 */
public class StupidDogSolver {
    // SDS yeet = new SDS(this, "right");
    // yeet.parseAndExecute("");
    /* START */
    public static class SDS implements SDSCommandScope {
        private SDS self;
        private Dogbot stupidDog;
        private int orientation = 0;
        private final java.util.List<SDSCommand> commands = new java.util.ArrayList<>();
        private final java.util.Map<String, String> data = new java.util.HashMap<>();
        private int dataIndex = 4;
        public SDS(Dogbot stupidDog, String startOrientation) {
            self = this;
            this.stupidDog = stupidDog;
            orientation = self.orientationFromString(startOrientation, 0);
        }
        @Override public SDS getSDS() {
            return this;
        }
        public void parse(String commandString) { commands.clear(); SDSCommandParser.parseToScope(commandString, this); }
        public void parseAndExecute(String commandString) { self.parse(commandString); self.execute(); }
        @Override public void addCommands(SDSCommand... commands) {
            for (SDSCommand command : commands) { command.setScope(this); this.commands.add(command); }
        }
        public void execute() {
            for (SDSCommand command : commands) command.executeAll();
        }
        @Override public java.util.Map<String, String> getAllData() {
            return data;
        }
        public void addData(String value) {
            self.addData(value, String.valueOf(dataIndex));
        }
        public void addData(String value, String key) {
            if (key == null) { self.addData(value); return; }
            data.put(key, value);
            dataIndex++;
        }
        @Override public String getData(String key) {
            return data.get(key);
        }
        public int getOrientation() { return orientation; }
        public String getOrientationString() { return self.stringFromOrientation(orientation); }
        private int orientationFromString(String str) {
            return self.orientationFromString(str, orientation);
        }
        private int orientationFromString(String str, int def) {
            switch (str) {
                case "u": case "up": return 0;
                case "d": case "down": return 2;
                case "l": case "left": return 3;
                case "r": case "right": return 1;
                default: return def;
            }
        }
        private String stringFromOrientation(int orientation) {
            switch (orientation % 4) {
                case 2: return "down";
                case 3: return "left";
                case 1: return "right";
                default: return "up";
            }
        }
        private void moveDirection(boolean onlyIfSafe, int direction, int iter) {
            if (direction < 0) direction = orientation;
            self.faceDirection(direction);
            while (iter-- > 0) if (!onlyIfSafe || stupidDog.isMovePossible()) stupidDog.move();
        }
        public void move() {
            self.move(false);
        }
        public void move(boolean onlyIfSafe) {
            self.moveDirection(onlyIfSafe, -1, 1);
        }
        public void move(String direction, int iter) {
            self.move(false, direction, iter);
        }
        public void move(boolean onlyIfSafe, String direction, int iter) {
            self.moveDirection(onlyIfSafe, self.orientationFromString(direction), iter);
        }
        public void move(String... directions) {
            self.move(false, directions);
        }
        public void move(boolean onlyIfSafe, String... directions) {
            for (String direction : directions) self.moveDirection(onlyIfSafe, self.orientationFromString(direction), 1);
        }
        public void face(String direction) {
            self.faceDirection(self.orientationFromString(direction));
        }
        public void faceDirection(int direction) {
            if ((orientation + 3) % 4 == direction) self.turnLeft();
            while (orientation != direction) self.turnRight();
        }
        public void turnLeft() {
            stupidDog.turnLeft();
            orientation = (orientation + 3) % 4;
        }
        public void turnRight() {
            stupidDog.turnRight();
            orientation = (orientation + 1) % 4;
        }
        public void pickUp() {
            stupidDog.pickUp();
        }
        public boolean isMovePossible() {
            return stupidDog.isMovePossible();
        }
        public String read() { return stupidDog.read(); }
        public String read(String direction) {
            self.faceDirection(self.orientationFromString(direction));
            return self.read();
        }
        public void write(String value) { stupidDog.write(value); }
        public void write(String direction, String value) {
            self.faceDirection(self.orientationFromString(direction));
            self.write(value);
        }
        public void rest() { stupidDog.rest(); }
    }
    private interface SDSCommandScope {
        SDS getSDS();
        void addCommands(SDSCommand... commands);
        void execute();
        java.util.Map<String, String> getAllData();
        void addData(String value);
        void addData(String value, String key);
        String getData(String key);
    }
    public static class MainCommandScope implements SDSCommandScope {
        public final MainCommandScope self;
        private final SDS sds;
        private final java.util.List<SDSCommand> commands = new java.util.ArrayList<>();
        private final java.util.Map<String, String> data = new java.util.HashMap<>();
        private int dataIndex = 4;
        public MainCommandScope(SDS sds) { this.self = this; this.sds = sds; }
        @Override public SDS getSDS() {
            return sds;
        }
        @Override public void addCommands(SDSCommand... commands) {
            for (SDSCommand command : commands) { command.setScope(this); this.commands.add(command); }
        }
        public void execute() {
            for (SDSCommand command : commands) command.executeAll();
        }
        @Override public java.util.Map<String, String> getAllData() {
            return data;
        }
        public void addData(String value) {
            self.addData(value, String.valueOf(dataIndex));
        }
        public void addData(String value, String key) {
            if (key == null) { self.addData(value); return; }
            data.put(key, value);
            dataIndex++;
        }
        @Override public String getData(String key) {
            return data.get(key);
        }
    }
    private static abstract class SDSCommand {
        public final SDSCommand self;
        private SDSCommandScope scope;
        private String reference = null;
        private String parameter = null;
        private int iterations = 1;
        private final String debugName;
        protected SDSCommand(String debugName) {
            this.self = this;
            this.debugName = debugName;
        }
        public SDS getSDS() {
            return scope.getSDS();
        }
        public SDSCommandScope getScope() {
            return scope;
        }
        public void setScope(SDSCommandScope scope) {
            this.scope = scope;
        }
        public String getRef() {
            return reference;
        }
        public void setRef(String reference) {
            this.reference = reference;
        }
        public String getRefValue() {
            SDSCommandScope scope = self.getScope();
            return scope.getData(reference);
        }
        public void setRefValue(String value) {
            SDSCommandScope scope = self.getScope();
            scope.addData(value, reference);
        }
        public String getParam() {
            return parameter;
        }
        public void setParam(String parameter) {
            this.parameter = parameter;
        }
        public String getParamOrRefValue() { return self.getParam() != null ? self.getParam() : self.getRefValue(); }
        public void setIterations(int iterations) {
            this.iterations = iterations;
        }
        public void executeAll() {
            for (int i = 0; i < iterations; i++) {
                System.out.println("Executing: " + debugName);
                self.execute();
            }
        }
        protected abstract void execute();
    }
    private static class SubCommand extends SDSCommand implements SDSCommandScope {
        private final java.util.List<SDSCommand> commands = new java.util.ArrayList<>();
        private SubCommand() {
            super("sub");
        }
        @Override public void addCommands(SDSCommand... commands) {
            for (SDSCommand command : commands) { command.setScope(this); this.commands.add(command); }
        }
        @Override public void execute() {
            for (SDSCommand command : commands) command.executeAll();
        }
        @Override public java.util.Map<String, String> getAllData() {
            SDSCommandScope scope = self.getScope();
            return scope.getAllData();
        }
        @Override public void addData(String value) {
            SDSCommandScope scope = self.getScope();
            scope.addData(value);
        }
        @Override public void addData(String value, String key) {
            SDSCommandScope scope = self.getScope();
            scope.addData(value, key);
        }
        @Override public String getData(String key) {
            SDSCommandScope scope = self.getScope();
            return scope.getData(key);
        }
    }
    private static class SDSCMove extends SDSCommand {
        SDSCMove() { super("move"); }
        @Override protected void execute() {
            SDS sds = self.getSDS();
            sds.faceDirection(sds.orientationFromString(self.getParamOrRefValue(), sds.orientation));
            sds.move(false);
        }
    }
    private static class SDSCMoveEnd extends SDSCommand {
        SDSCMoveEnd() { super("move_end"); }
        @Override protected void execute() {
            SDS sds = self.getSDS();
            sds.faceDirection(sds.orientationFromString(self.getParamOrRefValue(), sds.orientation));
            while (sds.isMovePossible()) sds.move(false);
        }
    }
    private static class SDSCMoveSave extends SDSCommand {
        SDSCMoveSave() { super("move_save"); }
        @Override protected void execute() {
            SDS sds = self.getSDS();
            sds.faceDirection(sds.orientationFromString(self.getParamOrRefValue(), sds.orientation));
            sds.move(true);
        }
    }
    private static class SDSCFace extends SDSCommand {
        SDSCFace() { super("face"); }
        @Override protected void execute() {
            SDS sds = self.getSDS();
            sds.faceDirection(sds.orientationFromString(self.getParamOrRefValue(), sds.orientation));
        }
    }
    private static class SDSCTurnLeft extends SDSCommand {
        SDSCTurnLeft() { super("turn_left"); }
        @Override protected void execute() {
            SDS sds = self.getSDS();
            sds.turnLeft();
        }
    }
    private static class SDSCTurnRight extends SDSCommand {
        SDSCTurnRight() { super("turn_right"); }
        @Override protected void execute() {
            SDS sds = self.getSDS();
            sds.turnRight();
        }
    }
    private static class SDSCPickUp extends SDSCommand {
        SDSCPickUp() { super("pick_up"); }
        @Override protected void execute() {
            SDS sds = self.getSDS();
            sds.pickUp();
        }
    }
    private static class SDSCRead extends SDSCommand {
        SDSCRead() { super("read"); }
        @Override protected void execute() {
            SDS sds = self.getSDS();
            self.setRefValue(sds.read());
        }
    }
    private static class SDSCWrite extends SDSCommand {
        SDSCWrite() { super("write"); }
        @Override protected void execute() {
            SDS sds = self.getSDS();
            sds.write(self.getParamOrRefValue());
        }
    }
    private static class SDSCCheckMove extends SDSCommand {
        SDSCCheckMove() { super("check_move"); }
        @Override protected void execute() {
            SDS sds = self.getSDS();
            self.setRefValue(sds.isMovePossible() ? "1" : "0");
        }
    }
    private static class SDSCRest extends SDSCommand {
        SDSCRest() { super("rest"); }
        @Override protected void execute() {
            SDS sds = self.getSDS();
            sds.rest();
        }
    }
    private enum SDSCommandTypes {
        MOVE("m"),
        MOVE_END("e"),
        MOVE_SAVE("s"),
        FACE("f"),
        TURN_LEFT("l"),
        TURN_RIGHT("r"),
        PICK_UP("p"),
        READ("d"),
        WRITE("w"),
        CHECK_MOVE("i"),
        REST("t");
        public final SDSCommandTypes self;
        public final String symbol;
        SDSCommandTypes(String symbol) {
            this.self = this;
            this.symbol = symbol;
        }
        private SDSCommand createInstance() {
            switch (this) {
                case MOVE: return new SDSCMove();
                case MOVE_END: return new SDSCMoveEnd();
                case MOVE_SAVE: return new SDSCMoveSave();
                case FACE: return new SDSCFace();
                case TURN_LEFT: return new SDSCTurnLeft();
                case TURN_RIGHT: return new SDSCTurnRight();
                case PICK_UP: return new SDSCPickUp();
                case READ: return new SDSCRead();
                case WRITE: return new SDSCWrite();
                case CHECK_MOVE: return new SDSCCheckMove();
                case REST: return new SDSCRest();
                default: return null;
            }
        }
        public SDSCommand createInstance(String val, String ref, int iter) {
            SDSCommand instance = self.createInstance();
            instance.setParam(val);
            instance.setRef(ref);
            instance.setIterations(iter);
            return instance;
        }
    }
    public static class SDSCommandParser {
        private static final String SDS_REGEX = "^(?:(?:(?:(?<command>[meslrpdwift])(?:\\((?<val>[^)]*)\\)|\\[(?<ref>[^]]*)\\]){0,2})(?<iter>[0-9]+)?)|(?<sub>\\{))";
        public static MainCommandScope parseAndExecute(SDS sds, String commandString) {
            MainCommandScope mainScope = new MainCommandScope(sds);
            SDSCommandParser.parseToScope(commandString, mainScope);
            mainScope.execute();
            return mainScope;
        }
        public static MainCommandScope parse(SDS sds, String commandString) {
            MainCommandScope mainScope = new MainCommandScope(sds);
            SDSCommandParser.parseToScope(commandString, mainScope);
            return mainScope;
        }
        private static SDSCommand createCommand(String commandSymbol, String val, String ref, int iter) {
            commandSymbol = commandSymbol.toLowerCase();
            for (SDSCommandTypes commandType : SDSCommandTypes.values()) if (java.util.Objects.equals(commandType.symbol, commandSymbol)) return commandType.createInstance(val, ref, iter);
            throw new SDSCommandParseException("Unknown symbol '" + commandSymbol + "'");
        }
        private static SubCommand parseSubCommand(String subString, int iter) {
            SubCommand subCommand = new SubCommand();
            subCommand.setIterations(iter);
            SDSCommandParser.parseToScope(subString, subCommand);
            return subCommand;
        }
        private static void parseToScope(String str, SDSCommandScope scope) {
            java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(SDS_REGEX, java.util.regex.Pattern.MULTILINE | java.util.regex.Pattern.CASE_INSENSITIVE);
            java.util.regex.Matcher matcher = pattern.matcher(str);
            while (matcher.find()) {
                if (matcher.group("sub") != null) {
                    char[] chars = str.toCharArray();
                    int i = 0, b = 0;
                    do {
                        if (chars[i] == '{') b++; else if (chars[i] == '}') b--;
                        i++;
                    } while (b != 0 && i <= chars.length);
                    int subLength = i - 2, iter = 0;
                    while (i < chars.length && Character.isDigit(chars[i])) iter = iter * 10 + Character.getNumericValue(chars[i++]);
                    scope.addCommands(SDSCommandParser.parseSubCommand(str.substring(1, subLength + 1), iter));
                    matcher = pattern.matcher(str = str.substring(i));
                } else {
                    String command = matcher.group("command");
                    String val = matcher.group("val");
                    String ref = matcher.group("ref");
                    String iter = matcher.group("iter");
                    System.out.println("Command found: " + command + ", " + val + ", " + ref + ", " + iter);
                    scope.addCommands(SDSCommandParser.createCommand(command, val, ref, iter == null ? 1 : Integer.parseInt(iter)));
                    str = str.substring(matcher.end());
                    matcher = pattern.matcher(str);
                }
            }
        }
    }
    public static class SDSCommandParseException extends RuntimeException {public SDSCommandParseException(String message) { super(message); }}
    /* END */
}