import codescape.Dogbot;


public class StupidDogSolverMini {
    // SDSM yeet = new SDSM(this, "STARTING ORIENTATION HERE (right|left|up|down)");
    /* Just copy and paste the code below (START to END) and use code above to run commands.
     * Example usage and documentation:
       public class MyDogbot extends Dogbot {
           
           public void run() {
               SDSM yeet = new SDSM(this, "right");
               
               // You can use these methods and many more:
               yeet.move("up", 3);
               yeet.move("down", "left");
               yeet.moveEnd("down");
               yeet.pickUp();
               yeet.face("right");
               yeet.passLaser();
               String result = yeet.read("down");
               yeet.write(result, "up");
               
               // Or you can do the same stuff but shorter:
               yeet.run("up", 3, "down", "left", "downend", "pick", "fright", "passlaser", "rddown", "~result");
               yeet.write(yeet.get("result"), "up");
               
               // Or even shorter:
               yeet.run("u", 3, "d", "l", "med", "p", "fr", "passlaser", "rdd", "~r", "wu", "~r");
               
               // Inside yeet.run(...) you can jump, move and repeat:
               // Use any positive number to repeat the previous command n-times
               // Use "@0", "@1", "@2", ... to jump to this command index.
               // Use ("?", A, B) to execute A only if Stupid Dog can move.
               // Use ("?!", A, B) to execute A only if Stupid Dog can NOT move.
               // Use "A>>B" with (A = "?" or "?!" or positive number) and (B = positive/negative number)
               //   to jump B-times relative to the current command (B>0 = forwards, B<0 = backwards).
               //   A = "?" -> only if Stupid Dog can move
               //   A = "?!" -> only if Stupid Dog can NOT move
               //   A = number -> do this A-times
               //   Example: run("right", "fdown", "?!>>-2") will move right but stop when dog is able to move down
               // Use "A>B" to do exactly the same as "A>>B" but instead of realtive to the current command jump absolute (like "@B").
           }
       
           // PASTE START TO END HERE
       }
     */
    /* START */
    public static class SDSM {
        private final SDSM self;
        private final Dogbot stupidDog;
        private int orientation = 0;
        private final java.util.Map<String, String> data = new java.util.HashMap<>();
        public SDSM(Dogbot stupidDog, String startOrientation) {
            self = this;
            this.stupidDog = stupidDog;
            orientation = self.orientationFromString(startOrientation);
        }
        private int orientationFromString(String str) {
            switch (str) {
                case "u": case "up": return 0;
                case "d": case "down": return 2;
                case "l": case "left": return 3;
                case "r": case "right": return 1;
                default: return orientation;
            }
        }
        private void moveDirection(boolean onlyIfSafe, int direction, int iter) {
            if (direction < 0) direction = orientation;
            self.faceDirection(direction);
            while (iter-- > 0) if (!onlyIfSafe || stupidDog.isMovePossible()) stupidDog.move();
        }
        private void faceDirection(int direction) { if ((orientation + 3) % 4 == direction) self.turnLeft(); while (orientation != direction) self.turnRight(); }
        /* METHODS YOU CAN USE */
        public String get(String ref) { return data.get(ref); }
        public void set(String ref, String val) { data.put(ref, val); }
        public void move() { self.move(false); }
        public void move(boolean onlyIfSafe) { self.moveDirection(onlyIfSafe, -1, 1); }
        public void move(String direction, int iter) { self.move(false, direction, iter); }
        public void move(boolean onlyIfSafe, String direction, int iter) { self.moveDirection(onlyIfSafe, self.orientationFromString(direction), iter); }
        public void move(String... directions) { self.move(false, directions); }
        public void move(boolean onlyIfSafe, String... directions) {
            for (String direction : directions) self.moveDirection(onlyIfSafe, self.orientationFromString(direction), 1);
        }
        public void moveEnd() { while (self.isMovePossible()) self.move(false); }
        public void moveEnd(String direction) { self.face(direction); while (self.isMovePossible()) self.move(false, direction); }
        public void face(String direction) { self.faceDirection(self.orientationFromString(direction)); }
        public void turn() { stupidDog.turnLeft(); stupidDog.turnLeft(); orientation = (orientation + 2) % 4; }
        public void turnLeft() { stupidDog.turnLeft(); orientation = (orientation + 3) % 4; }
        public void turnRight() { stupidDog.turnRight(); orientation = (orientation + 1) % 4; }
        public void pickUp() { stupidDog.pickUp(); }
        public boolean isMovePossible() { return stupidDog.isMovePossible(); }
        public String read() { return stupidDog.read(); }
        public String read(String direction) { self.faceDirection(self.orientationFromString(direction)); return self.read(); }
        public void readRef(String ref) { data.put(ref, self.read()); }
        public void readRef(String ref, String direction) { data.put(ref, self.read(direction)); }
        public void write(String value) { stupidDog.write(value); }
        public void write(String value, String direction) { self.faceDirection(self.orientationFromString(direction)); self.write(value); }
        public void writeRef(String ref) { self.write(data.get(ref)); }
        public void writeRef(String ref, String direction) { self.write(data.get(ref), direction); }
        public void rest() { stupidDog.rest(); }
        public void passLaser() { while (!self.isMovePossible()) self.rest(); self.move(false); }
        /* RUN ANY TYPE OF COMMAND + ALIASES + LOOPS + CONDITIONAL STATEMENTS */
        public void run(Object... commands) { self.run(commands, 0, new int[commands.length]); }
        private Object run(Object[] commands, int i, int[] iter) {
            if (i < 0 || i >= commands.length) return null;
            String c = java.util.Objects.toString(commands[i]);
            String n = i + 1 == commands.length ? "null" : java.util.Objects.toString(commands[i+1]);
            String v = "~".equals(n.substring(0, 1)) ? data.get(n.substring(1)) : n;
            c = c.toLowerCase();
            n = n.toLowerCase();
            switch (c) {
                case "m": case "move": self.move(); return self.run(commands, i + 1, iter);
                case "u": case "up":    case "mu": case "mup":    self.move("up");    return self.run(commands, i + 1, iter);
                case "d": case "down":  case "md": case "mdown":  self.move("down");  return self.run(commands, i + 1, iter);
                case "l": case "left":  case "ml": case "mleft":  self.move("left");  return self.run(commands, i + 1, iter);
                case "r": case "right": case "mr": case "mright": self.move("right"); return self.run(commands, i + 1, iter);
                case "me": case "moveend": self.moveEnd(); return self.run(commands, i + 1, iter);
                case "upend":    case "meu": case "meup":    self.moveEnd("up");    return self.run(commands, i + 1, iter);
                case "downend":  case "med": case "medown":  self.moveEnd("down");  return self.run(commands, i + 1, iter);
                case "leftend":  case "mel": case "meleft":  self.moveEnd("left");  return self.run(commands, i + 1, iter);
                case "rightend": case "mer": case "meright": self.moveEnd("right"); return self.run(commands, i + 1, iter);
                case "fu": case "fup":    self.face("up");    return self.run(commands, i + 1, iter);
                case "fd": case "fdown":  self.face("down");  return self.run(commands, i + 1, iter);
                case "fl": case "fleft":  self.face("left");  return self.run(commands, i + 1, iter);
                case "fr": case "fright": self.face("right"); return self.run(commands, i + 1, iter);
                case "t":  case "turn":   self.turn();      return self.run(commands, i + 1, iter);
                case "tl": case "tleft":  self.turnLeft();  return self.run(commands, i + 1, iter);
                case "tr": case "tright": self.turnRight(); return self.run(commands, i + 1, iter);
                case "p": case "pick": case "pickup": self.pickUp(); return self.run(commands, i + 1, iter);
                case "rst": case "rest": self.rest(); return self.run(commands, i + 1, iter);
                case "?": return self.run(commands, i + (self.isMovePossible() ? 1 : 2), iter);
                case "?!": return self.run(commands, i + (!self.isMovePossible() ? 1 : 2), iter);
                case "rd":  case "read":    self.readRef(n.substring(1));                   return self.run(commands, i + 2, iter);
                case "rdu": case "rdup":    self.readRef(n.substring(1), "up");    return self.run(commands, i + 2, iter);
                case "rdd": case "rddown":  self.readRef(n.substring(1), "down");  return self.run(commands, i + 2, iter);
                case "rdl": case "rdleft":  self.readRef(n.substring(1), "left");  return self.run(commands, i + 2, iter);
                case "rdr": case "rdright": self.readRef(n.substring(1), "right"); return self.run(commands, i + 2, iter);
                case "w":  case "write":  self.write(v);                   return self.run(commands, i + 2, iter);
                case "wu": case "wup":    self.write(v, "up");    return self.run(commands, i + 2, iter);
                case "wd": case "wdown":  self.write(v, "down");  return self.run(commands, i + 2, iter);
                case "wl": case "wleft":  self.write(v, "left");  return self.run(commands, i + 2, iter);
                case "wr": case "wright": self.write(v, "right"); return self.run(commands, i + 2, iter);
                case "passlaser":  self.passLaser(); return self.run(commands, i + 1, iter);
            }
            if ("@".equals(c.substring(0, 1))) return self.run(commands, i + 1, iter);
            if (c.contains("@")) {
                String[] parts = c.split("@");
                if ("?".equals(parts[0])) return self.run(commands, self.isMovePossible() ? self.indexOf("@" + parts[1], commands) : i+1, iter);
                if (self.isInteger(parts[0])) {
                    if (++iter[i] < Integer.parseInt(parts[0])) return self.run(commands, self.indexOf("@" + parts[1], commands), iter);
                    else { self.entryToValue(i, iter, 0); return self.run(commands, i + 1, iter); }
                }
            }
            if (c.contains(">>")) {
                String[] parts = c.split(">>");
                if (!self.isInteger(parts[1])) return null;
                if ("".equals(parts[0])) return self.run(commands, i+Integer.parseInt(parts[1]), iter);
                if ("?".equals(parts[0])) return self.run(commands, self.isMovePossible() ? i+Integer.parseInt(parts[1]) : i+1, iter);
                if ("?!".equals(parts[0])) return self.run(commands, !self.isMovePossible() ? i+Integer.parseInt(parts[1]) : i+1, iter);
                if (self.isInteger(parts[0])) {
                    if (++iter[i] < Integer.parseInt(parts[0])) return self.run(commands, i+Integer.parseInt(parts[1]), iter);
                    else { self.entryToValue(i, iter, 0); return self.run(commands, i + 1, iter); }
                }
            }
            if (c.contains(">")) {
                String[] parts = c.split(">");
                if (!self.isInteger(parts[1])) return null;
                if ("".equals(parts[0])) return self.run(commands, Integer.parseInt(parts[1]), iter);
                if ("?".equals(parts[0])) return self.run(commands, self.isMovePossible() ? Integer.parseInt(parts[1]) : i+1, iter);
                if ("?!".equals(parts[0])) return self.run(commands, self.isMovePossible() ? Integer.parseInt(parts[1]) : i+1, iter);
                if (self.isInteger(parts[0])) {
                    if (++iter[i] < Integer.parseInt(parts[0])) return self.run(commands, Integer.parseInt(parts[1]), iter);
                    else { self.entryToValue(i, iter, 0); return self.run(commands, i + 1, iter); }
                }
            }
            if (self.isInteger(c)) {
                if (++iter[i] < Integer.parseInt(c)) return self.run(commands, i - 1, iter);
                else { self.entryToValue(i, iter, 0); return self.run(commands, i + 1, iter); }
            }
            return null;
        }
        private <T> int indexOf(T element, T[] array) { if (element != null)  for (int i = 0; i < array.length; i++) if (element.equals(array[i])) return i; return -1; }
        private boolean isInteger(String str) { try { Integer.parseInt(str); return true; } catch (NumberFormatException e) { return false; } }
        private void entryToValue(int i, int[] a, int v) { while (a[i] < v) a[i]++; while (a[i] > v) a[i]--; }
        public void print(Object... objects) {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < objects.length; i++) { if (i != 0) s.append(", "); s.append(objects[i]); }
            System.out.println(s.toString());
        }
    }
    /* END */
}
